package com.example.sportingballapp.data

enum class AssetsState {
    LOADING, ERROR, LOADED
}