package com.example.sportingballapp.data

import androidx.annotation.Keep

@Keep
data class SportingBallSplashResponse(val url : String)