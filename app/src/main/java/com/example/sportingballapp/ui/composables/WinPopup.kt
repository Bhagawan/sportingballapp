package com.example.sportingballapp.ui.composables

import android.graphics.Color
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import com.example.sportingballapp.R
import com.example.sportingballapp.ui.theme.Transparent_Black

@Composable
fun WinPopup() {
    val winString = stringResource(id = R.string.win)

    AnimatedVisibility(visible = true,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        BoxWithConstraints(modifier = Modifier.fillMaxSize().background(Transparent_Black), contentAlignment = Alignment.Center) {
            val width = maxWidth
            val tSize = with(LocalDensity.current) { width.toPx() / 3.0f }

            val textPaintStroke = Paint().asFrameworkPaint().apply {
                textAlign = android.graphics.Paint.Align.CENTER
                isFakeBoldText = true
                isAntiAlias = true
                style = android.graphics.Paint.Style.STROKE
                textSize = tSize
                color = Color.WHITE
                strokeWidth = 10.0f
                strokeMiter = 10f
                strokeJoin = android.graphics.Paint.Join.ROUND
            }

            val textPaint = Paint().asFrameworkPaint().apply {
                textAlign = android.graphics.Paint.Align.CENTER
                isFakeBoldText = true
                isAntiAlias = true
                style = android.graphics.Paint.Style.FILL
                textSize = tSize
                color = Color.RED
            }

            Canvas(modifier = Modifier.fillMaxSize()) {
                drawIntoCanvas {
                    it.nativeCanvas.drawText(winString, it.nativeCanvas.width / 2.0f, it.nativeCanvas.height / 2.0f, textPaintStroke)
                    it.nativeCanvas.drawText(winString, it.nativeCanvas.width / 2.0f, it.nativeCanvas.height / 2.0f, textPaint)
                }
            }
        }
    }
}