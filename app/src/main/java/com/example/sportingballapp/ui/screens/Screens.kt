package com.example.sportingballapp.ui.screens

enum class Screens(val label: String) {
    SPLASH_SCREEN("splash"),
    NETWORK_ERROR_SCREEN("network_error"),
    WEB_VIEW("web_view"),
    PLAYER_SELECT_SCREEN("player_select"),
    GAME_SCREEN("game")
}