package com.example.sportingballapp.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.MotionEvent
import android.view.View
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import com.example.sportingballapp.data.Vector2D
import com.example.sportingballapp.data.stat.Assets
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.pow
import kotlin.math.sqrt

class GameView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0

    private var speed = 20.0f

    private var ballCoordinate = Vector2D(0.0f, 0.0f)
    private var ballRadius = 1.0f
    private var ballBitmap = Assets.ballBitmap?: Bitmap.createBitmap(1,1, Bitmap.Config.ARGB_8888)
    private val ballMoveVector = Vector2D(0.0f, 0.0f)

    private var enemyCoordinate = Vector2D(0.0f, 0.0f)
    private var enemySpeed = -1.0f

    private var arrowVisible = false
    private val arrowCoordinate = Vector2D(0.0f,0.0f)

    private var score = 0

    companion object {
        const val STATE_PAUSE = 0
        const val STATE_AIM = 1
        const val STATE_HIT = 2
    }

    private var currentState = STATE_AIM

    private var mInterface: GameInterface? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            ballRadius = mWidth / 10.0f
            ballCoordinate.x = mWidth / 2.0f
            ballCoordinate.y = mHeight - ballRadius
            speed = mHeight / 30.0f
            enemySpeed = speed / 4

            arrowCoordinate.x = mWidth / 2.0f
            arrowCoordinate.y = mHeight / 2.2f

            enemyCoordinate.x = mWidth / 2.0f
            enemyCoordinate.y = ballRadius
        }
    }

    override fun onDraw(canvas: Canvas) {
        updateEnemy()
        if(currentState == STATE_HIT) updateBall()
        if(arrowVisible) drawArrow(canvas)
        drawEnemy(canvas)
        drawBall(canvas)
        drawScore(canvas)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN,
            MotionEvent.ACTION_MOVE -> {
                if(currentState == STATE_AIM) updateArrow(event.x, event.y)
                return true
            }

            MotionEvent.ACTION_UP -> {
                if(currentState == STATE_AIM) hitBall()
                return true
            }
        }
        return false
    }

    //// Public

    fun setInterface(i: GameInterface) {
        mInterface = i
    }

    //// Private

    private fun drawBall(c: Canvas) {
        c.drawBitmap(ballBitmap,null,
            Rect((ballCoordinate.x - ballRadius).toInt(),
                (ballCoordinate.y - ballRadius).toInt(),
                (ballCoordinate.x + ballRadius).toInt(),
                (ballCoordinate.y + ballRadius).toInt()),
            Paint())
    }

    private fun drawArrow(c: Canvas) {
        val arrow = Vector2D(arrowCoordinate.x - ballCoordinate.x, arrowCoordinate.y - ballCoordinate.y)
        val arrLen = sqrt((arrow.x).pow(2) + (arrow.y).pow(2))
        val c1 = Vector2D(-arrow.x / arrLen * 40.0f, -arrow.y / arrLen * 40.0f)
        val c2 = Vector2D(-1.0f, -(c1.x / c1.y))
        val c2Len = sqrt((c2.x).pow(2) + (c2.y).pow(2))
        c2.x =  c2.x / c2Len * 20
        c2.y = c2.y / c2Len * 20
        val arrowLeft = Vector2D(c1.x + c2.x, c1.y - c2.y)
        val arrowRight = Vector2D(c1.x - c2.x, c1.y + c2.y)

        val p = Paint()
        p.color = Color.White.toArgb()
        p.strokeWidth = 14.0f
        c.drawLine(ballCoordinate.x, ballCoordinate.y, arrowCoordinate.x, arrowCoordinate.y, p)
        c.drawLine(arrowCoordinate.x, arrowCoordinate.y,arrowCoordinate.x + arrowLeft.x, arrowCoordinate.y + arrowLeft.y, p)
        c.drawLine(arrowCoordinate.x, arrowCoordinate.y,arrowCoordinate.x + arrowRight.x, arrowCoordinate.y + arrowRight.y, p)
    }

    private fun drawScore(c: Canvas) {
        val tSize = mWidth / 4.0f
        val p = Paint()
        p.color = Color.White.toArgb()
        p.textSize = tSize
        p.textAlign = Paint.Align.CENTER
        p.isFakeBoldText = true
        c.drawText(score.toString(), mWidth - tSize / 2, mHeight - 20.0f, p)
    }

    private fun drawEnemy(c: Canvas) {
        val p = Paint()
        p.color = Color.Red.toArgb()
        p.strokeWidth = 20.0f
        p.style = Paint.Style.STROKE
        c.drawCircle(enemyCoordinate.x, enemyCoordinate.y, ballRadius, p)
    }

    private fun updateArrow(x: Float, y: Float) {
        if(currentState == STATE_AIM) {
            if(!arrowVisible) arrowVisible = true
            arrowCoordinate.x = x.coerceAtLeast(10.0f).coerceAtMost(mWidth - 10.0f)
            arrowCoordinate.y = y.coerceAtLeast(mHeight / 2.2f)
                .coerceAtMost(ballCoordinate.y - ballRadius)
        }
    }

    private fun updateEnemy() {
        enemyCoordinate.x = (enemyCoordinate.x + enemySpeed).coerceAtLeast(ballRadius).coerceAtMost(mWidth - ballRadius)
        if(enemyCoordinate.x <= ballRadius || enemyCoordinate.x >= mWidth - ballRadius) enemySpeed *= -1
    }

    private fun updateBall() {
        ballCoordinate.x = (ballCoordinate.x + ballMoveVector.x).coerceAtLeast(ballRadius).coerceAtMost(mWidth - ballRadius)
        ballCoordinate.y += ballMoveVector.y

        if(ballCoordinate.x <= ballRadius || ballCoordinate.x >= mWidth - ballRadius) ballMoveVector.x *= -1

        val distToEnemy = sqrt((enemyCoordinate.x - ballCoordinate.x).pow(2) + (enemyCoordinate.y - ballCoordinate.y).pow(2))
        if(distToEnemy < ballRadius * 2) resetBall()
        else if(ballCoordinate.y < ballRadius) {
            if(++score >= 5 ) {
                currentState = STATE_PAUSE
                mInterface?.end()
            } else resetBall()
        }
    }

    private fun hitBall() {
        if(currentState == STATE_AIM) {
            currentState = STATE_HIT
            arrowVisible = false
            val arrow = Vector2D(arrowCoordinate.x - (mWidth / 2.0f), arrowCoordinate.y - (mHeight - ballRadius))
            val arrLen = sqrt((arrow.x).pow(2) + (arrow.y).pow(2))
            ballMoveVector.x = arrow.x / arrLen * speed
            ballMoveVector.y = arrow.y / arrLen * speed
        }
    }

    private fun resetBall() {
        currentState = STATE_AIM
        ballCoordinate.x = mWidth / 2.0f
        ballCoordinate.y = mHeight - ballRadius
    }

    interface GameInterface {
        fun end()
    }
}