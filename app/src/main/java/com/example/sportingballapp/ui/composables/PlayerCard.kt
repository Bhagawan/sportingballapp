package com.example.sportingballapp.ui.composables

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter

@Composable
fun PlayerCard(modifier: Modifier = Modifier, playerUrl: String) {
    Image(rememberImagePainter(playerUrl), contentDescription = null, contentScale = ContentScale.FillHeight,
        modifier = modifier
            .background(Color.Transparent, RoundedCornerShape(10.dp))
            .clipToBounds()
            .padding(10.dp))
}