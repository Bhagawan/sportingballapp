package com.example.sportingballapp.ui.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.sportingballapp.data.stat.UrlField
import com.example.sportingballapp.ui.composables.WinPopup
import com.example.sportingballapp.ui.theme.Green
import com.example.sportingballapp.ui.view.GameView

@Composable
fun GameScreen() {
    val viewModel = viewModel(GameScreenViewModel::class.java)
    val winPopup by viewModel.winPopup.collectAsState()
    var init  by rememberSaveable { mutableStateOf(true) }
    Box(modifier = Modifier
        .fillMaxSize()
        .background(Green)) {
        Image(rememberImagePainter(UrlField), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        AndroidView(factory = { GameView(it) },
            modifier = Modifier.fillMaxSize(),
            update = {
                if (init) {
                    it.setInterface(object: GameView.GameInterface {
                        override fun end() {
                            viewModel.win()
                        }
                    })
                    init = false
                }
            })
    }
    if(winPopup) WinPopup()
}