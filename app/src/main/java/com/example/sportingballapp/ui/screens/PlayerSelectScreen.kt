package com.example.sportingballapp.ui.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.snapping.rememberSnapFlingBehavior
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.sportingballapp.R
import com.example.sportingballapp.data.stat.AppData
import com.example.sportingballapp.data.stat.UrlBack
import com.example.sportingballapp.ui.composables.PlayerCard
import com.example.sportingballapp.ui.theme.Blue
import com.example.sportingballapp.util.navigation.Navigator
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Preview
@Composable
fun PlayerSelectScreen() {
    val coroutineScope = rememberCoroutineScope()
    val listState = rememberLazyListState(1)
    val leftButton by remember {
        derivedStateOf {
            listState.firstVisibleItemIndex > 0
        }
    }
    val rightButton by remember {
        derivedStateOf {
            listState.firstVisibleItemIndex < AppData.players.size - 1
        }
    }

    Image(rememberImagePainter(UrlBack), contentDescription = null, modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
    BoxWithConstraints(Modifier.fillMaxSize()) {
        val h = maxHeight
        val w = maxWidth
        LazyRow(state = listState, modifier = Modifier
            .fillMaxSize(),
            flingBehavior = rememberSnapFlingBehavior(listState)) {
            items(AppData.players) {
                PlayerCard(modifier = Modifier.size(w, h), playerUrl = it)
            }
        }
        Button(
            onClick = {
                Navigator.navigateTo(Screens.GAME_SCREEN)
            },
            modifier = Modifier
                .padding(bottom = h / 5)
                .align(Alignment.BottomCenter),
            colors = ButtonDefaults.buttonColors(
                containerColor = Color.White,
                contentColor = Color.Transparent
            ),
            shape = RoundedCornerShape(10.dp)
        ) {
            Text(stringResource(id = R.string.btn_play), fontSize = 25.sp, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center, color = Blue)
        }
        AnimatedVisibility(visible = leftButton,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.CenterStart) {
                Image(painterResource(id = R.drawable.round_chevron_left_24),
                    contentDescription = null,
                    modifier = Modifier
                        .size(50.dp)
                        .clickable(remember { MutableInteractionSource() }, null) {
                            if (listState.firstVisibleItemIndex > 0) {
                                coroutineScope.launch {
                                    listState.animateScrollToItem(
                                        listState.firstVisibleItemIndex - 1,
                                        0
                                    )
                                }
                            }
                        },
                    contentScale = ContentScale.FillBounds)
            }
        }

        AnimatedVisibility(visible = rightButton,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.CenterEnd) {
                Image(painterResource(id = R.drawable.round_chevron_right_24),
                    contentDescription = null,
                    modifier = Modifier
                        .size(50.dp)
                        .clickable(remember { MutableInteractionSource() }, null) {
                            if (listState.firstVisibleItemIndex < AppData.players.size - 1) {
                                coroutineScope.launch {
                                    listState.animateScrollToItem(
                                        listState.firstVisibleItemIndex + 1,
                                        0
                                    )
                                }
                            }
                        },
                    contentScale = ContentScale.FillBounds)
            }
        }
    }
}