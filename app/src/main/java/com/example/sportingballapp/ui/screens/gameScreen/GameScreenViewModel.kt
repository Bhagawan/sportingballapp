package com.example.sportingballapp.ui.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sportingballapp.ui.screens.Screens
import com.example.sportingballapp.util.navigation.Navigator
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class GameScreenViewModel: ViewModel() {
    private val _winPopup = MutableStateFlow(false)
    val winPopup = _winPopup.asStateFlow()

    fun win() {
        viewModelScope.launch {
            _winPopup.emit(true)
            delay(1000)
            Navigator.navigateTo(Screens.PLAYER_SELECT_SCREEN)
        }
    }
}