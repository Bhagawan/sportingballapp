package com.example.sportingballapp

import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sportingballapp.data.AssetsState
import com.example.sportingballapp.data.stat.AppData
import com.example.sportingballapp.data.stat.Assets
import com.example.sportingballapp.ui.screens.Screens
import com.example.sportingballapp.util.api.SportingBallServerClient
import com.example.sportingballapp.util.navigation.Navigator
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.TimeZone

class SportingBallMainViewModel: ViewModel() {
    private var request: Job? = null
    private val server = SportingBallServerClient.create()

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = server.getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> switchToApp()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                switchToApp()
                            }
                            else -> viewModelScope.launch {
                                AppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else switchToApp()
                } else switchToApp()
            }
        } catch (e: Exception) {
            switchToApp()
        }
    }

    private fun switchToApp() {
        Assets.assetsState.onEach {
            when(it) {
                AssetsState.ERROR -> Navigator.navigateTo(Screens.NETWORK_ERROR_SCREEN)
                AssetsState.LOADED -> Navigator.navigateTo(Screens.PLAYER_SELECT_SCREEN)
                else -> {}
            }
        }.launchIn(MainScope())
    }
}